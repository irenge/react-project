import React from 'react'

const Footer = props =>(
    <footer>
        <p>Contact</p>
        <p>
            <h4>{props.footerInf.mail}</h4>
            <h4>{props.footerInf.ville} - <a href={`tel:${props.footerInf.tel}`}>{props.footerInf.tel}</a></h4>
        </p>
    </footer>
)

export default Footer