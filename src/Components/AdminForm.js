import React, { Component } from 'react'

import * as firebase from 'firebase'


class AdminForm extends Component{

    constructor (props) {
        super(props)
        const data = this.props.data
        this.state = {data}
    }

    //Composant de mise à jour recta
    componentDidUpdate(){
        // Achaque fois qu'il y a modification on applele cette fx
        this.updateContent()
    }

    // methode pour mettre à jour les modifs dans la BDD
    updateContent = () =>{
        firebase.database().ref(`irenge/sections/${this.props.id}`).set({
           ...this.state.data
        })
    }
    //Methode pour Prendre en compte les modifications dans les sections
    handleChange = event =>{
        // Copie du state pour lui passer des valeurs
        const data = {...this.state.data}
        const name = event.target.name
        data[name] = event.target.value
        this.setState({data})
    }

    render(){
        return(
           <section>
                <input
                    name='color'
                    type='color'
                    value={this.state.data.color}
                    onChange={this.handleChange}
                />
               
                <textarea 
                  name='content'
                  rows='10'
                  value={this.state.data.content}
                  onChange={this.handleChange}
                />
           </section>
        )
 }
}


export default AdminForm