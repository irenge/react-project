import React from 'react'

const Maps = () =>(
    <iframe 
        title="Map location"
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2710.201490403165!2d-1.6203627840539105!3d47.212639679160496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4805ec99fc567bc7%3A0x3ab64d53276c9eae!2s4%20Rue%20de%20Morlaix%2C%2044800%20Saint-Herblain!5e0!3m2!1sfr!2sfr!4v1586513231230!5m2!1sfr!2sfr" 
        width="100%" 
        height="400" 
        frameBorder="0" 
        style={{border:0}} 
        allowFullScreen="" 
        aria-hidden="false" 
        tabIndex="0">
    </iframe>
)

export default Maps