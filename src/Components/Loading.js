import React from 'react'
import ReactLoading from "react-loading"
import '../css/loading.css'

const Loading = (props) =>(
    <div className="loading">
        <ReactLoading
            type='cylon'
        />
    </div>
)

export default Loading