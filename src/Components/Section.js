import React from 'react'
import marked from 'marked'

const Section = (props) =>{
    // Ajout du markdown pour le texte
    const renderText = text => {
        const renderText = marked(text)
        return { __html : renderText}
    }
    return(
        <section style={{backgroundColor : props.details.color}}>
            <div dangerouslySetInnerHTML={renderText(props.details.content)}/>
        </section>
    )
}

export default Section