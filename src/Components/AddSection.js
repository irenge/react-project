import React, { Component } from 'react'

import * as firebase from 'firebase'


class AddSection extends Component{
    
    state = {
        data: {
            color : '#000000',
            content: "Entrez votre texte svp..."
        }
    }

    //Methode pour Prendre en compte les modifications dans les sections
    handleChange = event =>{
        // Copie du state pour lui passer des valeurs
        const data = {...this.state.data}
        const name = event.target.name
        data[name] = event.target.value
        this.setState({data})
    }

    // Méthode pour ajouter une section 
    handleClick = event =>{
        firebase.database().ref(`irenge/sections`).push({
            ...this.state.data
        })
        this.setState({
            data:{
                color : '#000000',
                content : 'Entrez votre texte svp pour ajouter une section.'
            }
        })
    }
    render(){
        return(
           <section>
                <input
                    name='color'
                    type='color'
                    value={this.state.data.color}
                    onChange={this.handleChange}
                />
                <textarea 
                  name='content'
                  rows='10'
                  value={this.state.data.content}
                  onChange={this.handleChange}
               />
               <button onClick={this.handleClick}>Ajouter une section</button>
           </section>
        )
 }
}


export default AddSection