import React, { Component } from 'react'
import AdminForm from './AdminForm'
import AddSection from './AddSection'
import * as firebase from 'firebase'


class Admin extends Component{
    state = {
         uid: null,
         owner: null
    }

    componentDidMount (){
        const user = firebase.auth().currentUser
        if(user){
             this.authHandler(user)
        }
    }

    //Fonction d'authentification
    authenticate = event => {
        event.preventDefault()
        console.log(this.mail.value, this.password.value)
        firebase.auth().signInWithEmailAndPassword(this.mail.value,this.password.value)
            .then(user => this.authHandler(user) )      
    }

    authHandler = user => {
        
        user = firebase.auth().currentUser

        const ref = firebase.database().ref('irenge/owner')
        
        ref.on('value', snapshot => {
            this.setState({
                uid: user.uid,
                owner: snapshot.val()
            })
        })
    }

    //Fonction de déconnexion du logout
    logout = ()=> {
        firebase.auth().signOut()
            .then(this.setState({ uid : null}))
    }

    // Declaration du render d'authentification
    renderLogin = () =>(
        <div className="app">
                <header>
                    <h1>Administration</h1>
                </header>
                <section>
                    <form onSubmit={this.authenticate}>
                        <input placeholder ="Mail" type='mail' ref={input => this.mail = input} />
                        <input placeholder="Mot de passe" type='password' ref={input => this.password = input} />
                        <button type='submit'>Se connecter</button>
                    </form>
                </section>   
                <footer>
                   <button onClick={this.logout}>Se déconnecter</button>
                </footer>                       
        </div>
    )

    render(){
        //Checking de la connexion ou authentification
        if(!this.state.uid){
            return this.renderLogin()
        }
        
        //Checking de la connexion ou authentification
        if(this.state.uid !== this.state.owner){
            return this.renderLogin()
        }

        // Déclaration des sections
        const form = Object.keys(this.props.data.sections).map( key =>(
            <AdminForm key={key} id={key} data={this.props.data.sections[key]} />
        ))
        return(
           <div className="app">
               <header>
                   <h1>Administration</h1>
                   <button onClick={this.logout}>Se déconnecter</button>
               </header>
               {form}
               <AddSection />
               <footer>
                   <h1>Footer</h1>
               </footer> 

           </div>
        )
    }
}


export default Admin