import React, {Component} from 'react';
import ReactDOM from 'react-dom';

//React Router dom for web app
import 
{
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'


// Firebase
import * as firebase from 'firebase'
import config from './firebase'


//Components
import App from './Components/App';
import Admin from './Components/Admin';
import Loading from './Components/Loading'

//Css
import 'sanitize.css'
import './index.css';

//PWA
import * as serviceWorker from './serviceWorker';

class Root extends Component{
   
     // Initialisation du firebase
     constructor (){
      super()
      // On lui passe les params de conf si ce n'est le pardefaut
      if(!firebase.apps.length){
          firebase.initializeApp(config)  
      }
      // Initialiser le chargement à false
      this.state ={
          loading:true
      }
     
  }

  // cycle de vie pour chercher la ref des données
  componentWillMount(){
      const ref = firebase.database().ref('irenge')

      // Création du state au chargement
      // on récupère les valeurs dans la bdd
      ref.on('value', snapshot =>{
          this.setState({
              data : snapshot.val(),
              //chargement à false
              loading: false
          })
      })
  }

   render(){
      if(this.state.loading){
        return <Loading />
      }
      return(
        <Router>
          <Switch>
             <Route exact path='/' render={props =>(
               <App data={this.state.data} />
             )} />
             <Route exact path='/admin' render={props =>(
               <Admin data={this.state.data} />
             )} />
          </Switch>
        </Router>
      )
   }
}

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
